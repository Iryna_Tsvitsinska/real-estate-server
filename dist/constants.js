"use strict";
exports.__esModule = true;
exports.REAL_ESTATE_OBJECTS = void 0;
var realEstateObjectInterfaces_1 = require("./realEstateObjectInterfaces");
exports.REAL_ESTATE_OBJECTS = [
    {
        fullSquare: 100,
        activeSquare: 53,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        floor: 3,
        numberOfStoreys: 1,
        price: 52000,
        hasElevator: true,
        totalBuildingStoreys: 9,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.PRIVATE,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.FLAT,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        hasBalcon: true,
        id: 1,
        description: ""
    },
    {
        fullSquare: 20,
        activeSquare: 20,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        floor: 1,
        numberOfStoreys: 1,
        price: 6000,
        hasElevator: false,
        totalBuildingStoreys: 1,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.PRIVATE,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.GARAGE,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        hasCellar: true,
        hasServicePit: false,
        id: 2,
        description: ""
    },
    {
        fullSquare: 140,
        activeSquare: 70,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        floor: 1,
        numberOfStoreys: 2,
        price: 120000,
        hasElevator: false,
        totalBuildingStoreys: 2,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.PRIVATE,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.HOUSE,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        gardenSquare: 20,
        id: 3,
        description: ""
    },
    {
        fullSquare: 100,
        activeSquare: 80,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        floor: 2,
        numberOfStoreys: 1,
        price: 90000,
        hasElevator: true,
        totalBuildingStoreys: 20,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.LEGAL,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.OFFICE,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        hasParking: true,
        id: 4,
        description: ""
    },
    {
        fullSquare: 200,
        activeSquare: 150,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.NONE,
        floor: 1,
        numberOfStoreys: 1,
        price: 100000,
        hasElevator: false,
        totalBuildingStoreys: 1,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.LEGAL,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.WAREHOUSE,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        hasSecurity: true,
        id: 5,
        description: ""
    },
    {
        fullSquare: 120,
        activeSquare: 75,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        floor: 9,
        numberOfStoreys: 2,
        price: 90000,
        hasElevator: true,
        totalBuildingStoreys: 9,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.PRIVATE,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.FLAT,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        hasBalcon: true,
        id: 6,
        description: ""
    },
    {
        fullSquare: 200,
        activeSquare: 120,
        gasType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        waterType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        sewerageType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        electricityType: realEstateObjectInterfaces_1.UtilityConnectionType.CENTRALIZED,
        heatingType: realEstateObjectInterfaces_1.UtilityConnectionType.OWN,
        floor: 1,
        numberOfStoreys: 2,
        price: 200000,
        hasElevator: false,
        totalBuildingStoreys: 2,
        ownershipType: realEstateObjectInterfaces_1.OwnershipType.PRIVATE,
        propertyType: realEstateObjectInterfaces_1.RealEstateType.HOUSE,
        propertyState: realEstateObjectInterfaces_1.RealEstateStateType.SALE,
        gardenSquare: 10,
        id: 7,
        description: ""
    },
];
