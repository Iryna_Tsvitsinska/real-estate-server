"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var multer_1 = __importDefault(require("multer"));
var path_1 = __importDefault(require("path"));
var cors_1 = __importDefault(require("cors"));
var utility_1 = require("./utility");
var fs_1 = require("fs");
var app = (0, express_1["default"])();
// const upload = multer({ dest: "uploads/" });
var port = 7000;
app.use((0, cors_1["default"])());
app.use(express_1["default"].json()); // to support JSON-encoded bodies
app.use(express_1["default"].urlencoded({
    extended: true
})); // to support URL-encoded bodies
var storage = multer_1["default"].diskStorage({
    destination: function (req, file, cb) {
        cb(null, "uploads/");
    },
    filename: function (req, file, cb) {
        var fileName = Date.now() + path_1["default"].extname(file.originalname);
        cb(null, fileName);
    }
});
var upload = (0, multer_1["default"])({ storage: storage });
app.post("/upload", upload.single("image"), function (req, res, next) {
    // console.log(req.body.realEstateId, "TEST");
    var realEstateId = req.body.realEstateId;
    // console.log(req.file.filename);
    var fileName = req.file.filename;
    // console.log(fileName);
    // req.file - файл `avatar`
    // req.body сохранит текстовые поля, если они будут
    getImages()
        // .then(data=> console.log(data))
        .then(function (data) {
        // console.log(data);
        var newInfoImage = {
            imageId: (0, utility_1.getnewIdImage)(data),
            name: fileName,
            idRealEstate: Number(realEstateId)
        };
        setImages(__spreadArray(__spreadArray([], data, true), [newInfoImage], false));
        return newInfoImage.imageId;
    })
        .then(function (imageId) {
        res.json({
            fileName: fileName,
            imageId: imageId
        });
    });
});
app.use("/uploads", express_1["default"].static("uploads"));
app.get("/", function (req, res) {
    res.send("hello world");
});
app.get("/initial", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var list, images, newList, initialState;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, getRealEstateObjects()];
            case 1:
                list = _a.sent();
                return [4 /*yield*/, getImages()];
            case 2:
                images = _a.sent();
                newList = list.map(function (el) {
                    var imgs = images
                        .filter(function (item) { return item.idRealEstate === el.id; })
                        .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                    // console.log(imgs);
                    return __assign(__assign({}, el), { images: imgs });
                });
                initialState = {
                    currentList: newList,
                    totalLimits: {
                        minPrice: (0, utility_1.getMinValuePrice)(list),
                        maxPrice: (0, utility_1.getMaxValuePrice)(list),
                        minSquare: (0, utility_1.getMinValueSquare)(list),
                        maxSquare: (0, utility_1.getMaxValueSquare)(list)
                    }
                };
                res.json(initialState);
                return [2 /*return*/];
        }
    });
}); });
app.post("/sort", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, minSquareUser, maxSquareUser, minPriceUser, maxPriceUser, activeCategories, realEstateObjects, allImages, list, searchLimits, newList, currentList, data;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, minSquareUser = _a.minSquareUser, maxSquareUser = _a.maxSquareUser, minPriceUser = _a.minPriceUser, maxPriceUser = _a.maxPriceUser, activeCategories = _a.activeCategories;
                return [4 /*yield*/, getRealEstateObjects()];
            case 1:
                realEstateObjects = _b.sent();
                return [4 /*yield*/, getImages()];
            case 2:
                allImages = _b.sent();
                list = activeCategories.length > 0
                    ? realEstateObjects.filter(function (realEstateObject) {
                        return activeCategories.includes(realEstateObject.propertyType);
                    })
                    : realEstateObjects;
                searchLimits = {
                    minSquare: (0, utility_1.getMinValueSquare)(list),
                    maxSquare: (0, utility_1.getMaxValueSquare)(list),
                    minPrice: (0, utility_1.getMinValuePrice)(list),
                    maxPrice: (0, utility_1.getMaxValuePrice)(list)
                };
                newList = list.filter(function (realEstateObject) {
                    return realEstateObject.price >= minPriceUser &&
                        realEstateObject.price <= maxPriceUser &&
                        realEstateObject.fullSquare >= minSquareUser &&
                        realEstateObject.fullSquare <= maxSquareUser;
                });
                currentList = newList.map(function (el) {
                    var imgs = allImages
                        .filter(function (item) { return item.idRealEstate === el.id; })
                        .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                    // console.log(imgs);
                    return __assign(__assign({}, el), { images: imgs });
                });
                data = { sortList: currentList, searchLimits: searchLimits };
                res.json(data);
                return [2 /*return*/];
        }
    });
}); });
app.post("/addObjectRealEstate", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var objectInfo, images;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                objectInfo = req.body;
                return [4 /*yield*/, getImages()];
            case 1:
                images = _a.sent();
                getRealEstateObjects()
                    .then(function (data) {
                    var id = (0, utility_1.getnewIdValue)(data);
                    var newImages = objectInfo.images.map(function (img) { return ({
                        imageId: img.imageId,
                        name: img.fileName,
                        idRealEstate: id
                    }); });
                    var imgs = images.map(function (el) {
                        var currentImg = newImages.find(function (imgInfo) { return el.imageId === imgInfo.imageId; });
                        return currentImg ? currentImg : el;
                    });
                    setImages(imgs);
                    delete objectInfo["images"];
                    data.push(__assign(__assign({}, objectInfo), { id: id }));
                    setRealEstateObjects(data);
                    var currentList = data.map(function (el) {
                        var images = imgs
                            .filter(function (item) { return item.idRealEstate === el.id; })
                            .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                        console.log(images);
                        return __assign(__assign({}, el), { images: images });
                    });
                    return currentList;
                })
                    .then(function (list) { return ({
                    currentList: list,
                    totalLimits: {
                        minPrice: (0, utility_1.getMinValuePrice)(list),
                        maxPrice: (0, utility_1.getMaxValuePrice)(list),
                        minSquare: (0, utility_1.getMinValueSquare)(list),
                        maxSquare: (0, utility_1.getMaxValueSquare)(list)
                    }
                }); })
                    .then(function (data) { return res.json(data); });
                return [2 /*return*/];
        }
    });
}); });
app.post("/editRealEstateObject", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        id = req.body.id;
        console.log(id);
        getRealEstateObjects()
            .then(function (list) { return list.find(function (el) { return el.id === id; }); })
            .then(function (object) {
            var images = getImages()
                .then(function (data) {
                var images = data
                    .filter(function (item) { return item.idRealEstate === object.id; })
                    .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                return __assign(__assign({}, object), { images: images });
            })
                .then(function (data) {
                res.json(data);
            });
        });
        return [2 /*return*/];
    });
}); });
app.post("/showRealEstateObject", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id;
    return __generator(this, function (_a) {
        id = req.body.id;
        console.log(id);
        getRealEstateObjects()
            .then(function (list) { return list.find(function (el) { return el.id === id; }); })
            .then(function (object) {
            var images = getImages()
                .then(function (data) {
                var images = data
                    .filter(function (item) { return item.idRealEstate === object.id; })
                    .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                return __assign(__assign({}, object), { images: images });
            })
                .then(function (data) {
                res.json(data);
            });
        });
        return [2 /*return*/];
    });
}); });
app.post("/deleteObjectRealEstate", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var delRealEstateObjectId, images, newImages;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                delRealEstateObjectId = req.body.delRealEstateObjectId;
                console.log(delRealEstateObjectId);
                return [4 /*yield*/, getImages()];
            case 1:
                images = _a.sent();
                images.forEach(function (el) {
                    if (el.idRealEstate === delRealEstateObjectId) {
                        deleteFoto(el.name);
                    }
                });
                newImages = images.filter(function (img) { return img.idRealEstate !== delRealEstateObjectId; });
                setImages(newImages);
                getRealEstateObjects()
                    .then(function (list) { return list.filter(function (el) { return el.id !== delRealEstateObjectId; }); })
                    .then(function (data) {
                    setRealEstateObjects(data);
                    var currentList = data.map(function (el) {
                        var imgs = images
                            .filter(function (item) { return item.idRealEstate === el.id; })
                            .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                        // console.log(imgs);
                        return __assign(__assign({}, el), { images: imgs });
                    });
                    return currentList;
                })
                    .then(function (list) { return ({
                    currentList: list,
                    totalLimits: {
                        minPrice: (0, utility_1.getMinValuePrice)(list),
                        maxPrice: (0, utility_1.getMaxValuePrice)(list),
                        minSquare: (0, utility_1.getMinValueSquare)(list),
                        maxSquare: (0, utility_1.getMaxValueSquare)(list)
                    }
                }); })
                    .then(function (data) {
                    res.json(data);
                });
                return [2 /*return*/];
        }
    });
}); });
app.post("/saveChangesObjectRealEstate", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var realEstateObject, images, newImages, imgs;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                realEstateObject = req.body;
                console.log(realEstateObject.id);
                return [4 /*yield*/, getImages()];
            case 1:
                images = _a.sent();
                newImages = realEstateObject.images.map(function (img) { return ({
                    imageId: img.imageId,
                    name: img.fileName,
                    idRealEstate: realEstateObject.id
                }); });
                imgs = images
                    .filter(function (el) { return !newImages.find(function (img) { return img.imageId === el.imageId; }); })
                    .concat(newImages);
                console.log(imgs);
                setImages(imgs);
                delete realEstateObject["images"];
                getRealEstateObjects()
                    .then(function (list) {
                    return list.map(function (item) {
                        return item.id === realEstateObject.id ? realEstateObject : item;
                    });
                })
                    .then(function (data) {
                    setRealEstateObjects(data);
                    var currentList = data.map(function (el) {
                        var imgs = images
                            .filter(function (item) { return item.idRealEstate === el.id; })
                            .map(function (item) { return ({ fileName: item.name, imageId: item.imageId }); });
                        // console.log(imgs);
                        return __assign(__assign({}, el), { images: imgs });
                    });
                    return currentList;
                })
                    .then(function (list) { return ({
                    currentList: list,
                    totalLimits: {
                        minPrice: (0, utility_1.getMinValuePrice)(list),
                        maxPrice: (0, utility_1.getMaxValuePrice)(list),
                        minSquare: (0, utility_1.getMinValueSquare)(list),
                        maxSquare: (0, utility_1.getMaxValueSquare)(list)
                    }
                }); })
                    .then(function (data) {
                    res.json(data);
                });
                return [2 /*return*/];
        }
    });
}); });
app.post("/deleteImage", function (req, res) {
    var imageId = req.body.imageId;
    getImageNameByImageId(imageId).then(function (name) {
        deleteFoto(name);
        getImages()
            .then(function (data) { return data.filter(function (el) { return el.imageId !== imageId; }); })
            .then(function (data) {
            setImages(data);
            return imageId;
        })
            .then(function (id) {
            res.json({ res: id + " " });
        });
    });
});
// app.get("/initial", function (req, res) {
//   res.json({ name: "Vovsa", age: 42 });
// });
app.listen(port, function () {
    console.log("Etsy parser server running at http://localhost:" + port);
});
function getRealEstateObjects() {
    return fs_1.promises
        .readFile("data/realestateobjects.json", "utf8")
        .then(function (f) { return JSON.parse(f); });
}
function setRealEstateObjects(objects) {
    return fs_1.promises
        .writeFile("data/realestateobjects.json", JSON.stringify(objects))
        .then(function () { return objects; });
}
function getImages() {
    return fs_1.promises.readFile("data/imagesInfo.json", "utf8").then(function (f) { return JSON.parse(f); });
}
function setImages(objects) {
    return fs_1.promises
        .writeFile("data/imagesInfo.json", JSON.stringify(objects))
        .then(function () { return objects; });
}
function deleteFoto(name) {
    var dir = "uploads/" + name;
    return fs_1.promises.unlink(dir);
}
function getImageNameByImageId(id) {
    var st;
    return getImages()
        .then(function (data) {
        return data.find(function (imageInfo) { return imageInfo.imageId === id; });
    })
        .then(function (imageInfo) {
        st = imageInfo.name;
        console.log(st);
        return st;
    });
}
// getImageNameById(1);
function getImagesByRealEstateId(id) {
    return getImages().then(function (data) {
        return data.filter(function (el) { return el.idRealEstate === id; });
    });
}
// getRealEstateObjects()
//   .then((data) => {
//     const newList = data.map((elem) => {
//       const newEl = { ...elem };
//       delete newEl["images"];
//       return newEl;
//     });
//     // const newList = data.map((elem) => ({ ...elem, images: [] }));
//     return newList;
//   })
//   .then((data) => {
//     setRealEstateObjects(data);
//   });
// setRealEstateObjects(REAL_ESTATE_OBJECTS);
