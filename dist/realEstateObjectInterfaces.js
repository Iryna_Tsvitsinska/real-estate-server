"use strict";
exports.__esModule = true;
exports.RealEstateStateType = exports.RealEstateType = exports.OwnershipType = exports.UtilityConnectionType = void 0;
var UtilityConnectionType;
(function (UtilityConnectionType) {
    UtilityConnectionType[UtilityConnectionType["NONE"] = -1] = "NONE";
    UtilityConnectionType[UtilityConnectionType["OWN"] = 0] = "OWN";
    UtilityConnectionType[UtilityConnectionType["CENTRALIZED"] = 1] = "CENTRALIZED";
})(UtilityConnectionType = exports.UtilityConnectionType || (exports.UtilityConnectionType = {}));
var OwnershipType;
(function (OwnershipType) {
    OwnershipType[OwnershipType["PRIVATE"] = 0] = "PRIVATE";
    OwnershipType[OwnershipType["LEGAL"] = 1] = "LEGAL";
    OwnershipType[OwnershipType["RENT"] = 2] = "RENT";
    OwnershipType[OwnershipType["MUNICIPAL"] = 3] = "MUNICIPAL";
})(OwnershipType = exports.OwnershipType || (exports.OwnershipType = {}));
var RealEstateType;
(function (RealEstateType) {
    RealEstateType[RealEstateType["GARAGE"] = 0] = "GARAGE";
    RealEstateType[RealEstateType["WAREHOUSE"] = 1] = "WAREHOUSE";
    RealEstateType[RealEstateType["OFFICE"] = 2] = "OFFICE";
    RealEstateType[RealEstateType["FLAT"] = 3] = "FLAT";
    RealEstateType[RealEstateType["HOUSE"] = 4] = "HOUSE";
})(RealEstateType = exports.RealEstateType || (exports.RealEstateType = {}));
var RealEstateStateType;
(function (RealEstateStateType) {
    RealEstateStateType[RealEstateStateType["RENT"] = 0] = "RENT";
    RealEstateStateType[RealEstateStateType["SALE"] = 1] = "SALE";
})(RealEstateStateType = exports.RealEstateStateType || (exports.RealEstateStateType = {}));
