"use strict";
exports.__esModule = true;
exports.getnewIdImage = exports.getnewIdValue = exports.getMinValuePrice = exports.getMinValueSquare = exports.getMaxValueSquare = exports.getMaxValuePrice = void 0;
var getMaxValuePrice = function (list) {
    var maxElem = list.reduce(function (res, current) {
        return res.price > current.price ? res : current;
    });
    return maxElem.price;
};
exports.getMaxValuePrice = getMaxValuePrice;
var getMaxValueSquare = function (list) {
    var maxElem = list.reduce(function (res, current) {
        return res.fullSquare > current.fullSquare ? res : current;
    });
    return maxElem.fullSquare;
};
exports.getMaxValueSquare = getMaxValueSquare;
var getMinValueSquare = function (list) {
    var maxElem = list.reduce(function (res, current) {
        return res.fullSquare < current.fullSquare ? res : current;
    });
    return maxElem.fullSquare;
};
exports.getMinValueSquare = getMinValueSquare;
var getMinValuePrice = function (list) {
    var maxElem = list.reduce(function (res, current) {
        return res.price < current.price ? res : current;
    });
    return maxElem.price;
};
exports.getMinValuePrice = getMinValuePrice;
var getnewIdValue = function (list) {
    return list.reduce(function (maxId, item) { return Math.max(maxId, item.id); }, 0) + 1;
};
exports.getnewIdValue = getnewIdValue;
var getnewIdImage = function (list) {
    return list.reduce(function (maxId, item) { return Math.max(maxId, item.imageId); }, 0) + 1;
};
exports.getnewIdImage = getnewIdImage;
