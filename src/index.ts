import express from "express";
import multer from "multer";
import path from "path";
import cors from "cors";
import { REAL_ESTATE_OBJECTS } from "./constants";
import {
  getMaxValuePrice,
  getMaxValueSquare,
  getMinValuePrice,
  getMinValueSquare,
  getnewIdImage,
  getnewIdValue,
} from "./utility";
import {
  ImageInfoConfig,
  RealEstateObjectTypeConfig,
} from "./realEstateObjectInterfaces";
import { promises as fs } from "fs";

const app = express();
// const upload = multer({ dest: "uploads/" });
const port = 7000;

app.use(cors());
app.use(express.json()); // to support JSON-encoded bodies
app.use(
  express.urlencoded({
    extended: true,
  })
); // to support URL-encoded bodies

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/");
  },

  filename: function (req, file, cb) {
    const fileName = Date.now() + path.extname(file.originalname);
    cb(null, fileName);
  },
});

const upload = multer({ storage });

app.post("/upload", upload.single("image"), function (req, res, next) {
  // console.log(req.body.realEstateId, "TEST");
  const { realEstateId } = req.body;
  // console.log(req.file.filename);
  const fileName = req.file.filename;

  // console.log(fileName);
  // req.file - файл `avatar`
  // req.body сохранит текстовые поля, если они будут

  getImages()
    // .then(data=> console.log(data))
    .then((data) => {
      // console.log(data);
      const newInfoImage = {
        imageId: getnewIdImage(data),
        name: fileName,
        idRealEstate: Number(realEstateId), //???
      };
      setImages([...data, newInfoImage]);
      return newInfoImage.imageId;
    })
    .then((imageId) => {
      res.json({
        fileName: fileName,
        imageId: imageId,
      });
    });
});

app.use("/uploads", express.static("uploads"));

app.get("/", function (req, res) {
  res.send("hello world");
});

app.get("/initial", async (req, res) => {
  const list = await getRealEstateObjects();
  const images = await getImages();

  const newList = list.map((el) => {
    const imgs = images
      .filter((item) => item.idRealEstate === el.id)
      .map((item) => ({ fileName: item.name, imageId: item.imageId }));
    // console.log(imgs);
    return { ...el, images: imgs };
  });
  const initialState = {
    currentList: newList,
    totalLimits: {
      minPrice: getMinValuePrice(list),
      maxPrice: getMaxValuePrice(list),
      minSquare: getMinValueSquare(list),
      maxSquare: getMaxValueSquare(list),
    },
  };

  res.json(initialState);
});

app.post("/sort", async (req, res) => {
  const {
    minSquareUser,
    maxSquareUser,
    minPriceUser,
    maxPriceUser,
    activeCategories,
  } = req.body;

  const realEstateObjects = await getRealEstateObjects();
  const allImages = await getImages();

  const list =
    activeCategories.length > 0
      ? realEstateObjects.filter((realEstateObject) =>
          activeCategories.includes(realEstateObject.propertyType)
        )
      : realEstateObjects;

  const searchLimits = {
    minSquare: getMinValueSquare(list),
    maxSquare: getMaxValueSquare(list),
    minPrice: getMinValuePrice(list),
    maxPrice: getMaxValuePrice(list),
  };

  const newList = list.filter(
    (realEstateObject) =>
      realEstateObject.price >= minPriceUser &&
      realEstateObject.price <= maxPriceUser &&
      realEstateObject.fullSquare >= minSquareUser &&
      realEstateObject.fullSquare <= maxSquareUser
  );

  const currentList = newList.map((el) => {
    const imgs = allImages
      .filter((item) => item.idRealEstate === el.id)
      .map((item) => ({ fileName: item.name, imageId: item.imageId }));
    // console.log(imgs);
    return { ...el, images: imgs };
  });

  const data = { sortList: currentList, searchLimits };
  res.json(data);
});

app.post("/addObjectRealEstate", async (req, res) => {
  const objectInfo = req.body;
  const images = await getImages();

  getRealEstateObjects()
    .then((data) => {
      const id = getnewIdValue(data);

      const newImages = objectInfo.images.map((img) => ({
        imageId: img.imageId,
        name: img.fileName,
        idRealEstate: id,
      }));
      const imgs = images.map((el) => {
        const currentImg = newImages.find(
          (imgInfo) => el.imageId === imgInfo.imageId
        );
        return currentImg ? currentImg : el;
      });
      setImages(imgs);
      delete objectInfo["images"];
      data.push({ ...objectInfo, id });
      setRealEstateObjects(data);
      const currentList = data.map((el) => {
        const images = imgs
          .filter((item) => item.idRealEstate === el.id)
          .map((item) => ({ fileName: item.name, imageId: item.imageId }));
        console.log(images);
        return { ...el, images };
      });
      return currentList;
    })
    .then((list) => ({
      currentList: list,
      totalLimits: {
        minPrice: getMinValuePrice(list),
        maxPrice: getMaxValuePrice(list),
        minSquare: getMinValueSquare(list),
        maxSquare: getMaxValueSquare(list),
      },
    }))
    .then((data) => res.json(data));
});

app.post("/editRealEstateObject", async (req, res) => {
  const { id } = req.body;
  console.log(id);
  getRealEstateObjects()
    .then((list) => list.find((el) => el.id === id))
    .then((object) => {
      const images = getImages()
        .then((data) => {
          const images = data
            .filter((item) => item.idRealEstate === object.id)
            .map((item) => ({ fileName: item.name, imageId: item.imageId }));

          return { ...object, images };
        })
        .then((data) => {
          res.json(data);
        });
    });
});
app.post("/showRealEstateObject", async (req, res) => {
  const { id } = req.body;
  console.log(id);
  getRealEstateObjects()
    .then((list) => list.find((el) => el.id === id))
    .then((object) => {
      const images = getImages()
        .then((data) => {
          const images = data
            .filter((item) => item.idRealEstate === object.id)
            .map((item) => ({ fileName: item.name, imageId: item.imageId }));

          return { ...object, images };
        })
        .then((data) => {
          res.json(data);
        });
    });
});

app.post("/deleteObjectRealEstate", async (req, res) => {
  const { delRealEstateObjectId } = req.body;
  console.log(delRealEstateObjectId);
  const images = await getImages();

  images.forEach((el) => {
    if (el.idRealEstate === delRealEstateObjectId) {
      deleteFoto(el.name);
    }
  });
  const newImages = images.filter(
    (img) => img.idRealEstate !== delRealEstateObjectId
  );
  setImages(newImages);

  getRealEstateObjects()
    .then((list) => list.filter((el) => el.id !== delRealEstateObjectId))
    .then((data) => {
      setRealEstateObjects(data);
      const currentList = data.map((el) => {
        const imgs = images
          .filter((item) => item.idRealEstate === el.id)
          .map((item) => ({ fileName: item.name, imageId: item.imageId }));
        // console.log(imgs);
        return { ...el, images: imgs };
      });
      return currentList;
    })
    .then((list) => ({
      currentList: list,
      totalLimits: {
        minPrice: getMinValuePrice(list),
        maxPrice: getMaxValuePrice(list),
        minSquare: getMinValueSquare(list),
        maxSquare: getMaxValueSquare(list),
      },
    }))
    .then((data) => {
      res.json(data);
    });
});
app.post("/saveChangesObjectRealEstate", async (req, res) => {
  const realEstateObject = req.body;
  console.log(realEstateObject.id);

  const images = await getImages();
  // console.log(images);
  const newImages = realEstateObject.images.map((img) => ({
    imageId: img.imageId,
    name: img.fileName,
    idRealEstate: realEstateObject.id,
  }));

  const imgs = images
    .filter((el) => !newImages.find((img) => img.imageId === el.imageId))
    .concat(newImages);
  console.log(imgs);
  setImages(imgs);
  delete realEstateObject["images"];

  getRealEstateObjects()
    .then((list) =>
      list.map((item) =>
        item.id === realEstateObject.id ? realEstateObject : item
      )
    )
    .then((data) => {
      setRealEstateObjects(data);
      const currentList = data.map((el) => {
        const imgs = images
          .filter((item) => item.idRealEstate === el.id)
          .map((item) => ({ fileName: item.name, imageId: item.imageId }));
        // console.log(imgs);
        return { ...el, images: imgs };
      });
      return currentList;
    })
    .then((list) => ({
      currentList: list,
      totalLimits: {
        minPrice: getMinValuePrice(list),
        maxPrice: getMaxValuePrice(list),
        minSquare: getMinValueSquare(list),
        maxSquare: getMaxValueSquare(list),
      },
    }))
    .then((data) => {
      res.json(data);
    });
});

app.post("/deleteImage", function (req, res) {
  const { imageId } = req.body;
  getImageNameByImageId(imageId).then((name) => {
    deleteFoto(name);
    getImages()
      .then((data) => data.filter((el) => el.imageId !== imageId))
      .then((data) => {
        setImages(data);
        return imageId;
      })
      .then((id) => {
        res.json({ res: `${id} ` });
      });
  });
});

// app.get("/initial", function (req, res) {
//   res.json({ name: "Vovsa", age: 42 });
// });

app.listen(port, () => {
  console.log(`Etsy parser server running at http://localhost:${port}`);
});

function getRealEstateObjects(): Promise<RealEstateObjectTypeConfig[]> {
  return fs
    .readFile("data/realestateobjects.json", "utf8")
    .then((f) => JSON.parse(f));
}

function setRealEstateObjects(
  objects: RealEstateObjectTypeConfig[]
): Promise<RealEstateObjectTypeConfig[]> {
  return fs
    .writeFile("data/realestateobjects.json", JSON.stringify(objects))
    .then(() => objects);
}

function getImages(): Promise<ImageInfoConfig[]> {
  return fs.readFile("data/imagesInfo.json", "utf8").then((f) => JSON.parse(f));
}

function setImages(objects: ImageInfoConfig[]): Promise<ImageInfoConfig[]> {
  return fs
    .writeFile("data/imagesInfo.json", JSON.stringify(objects))
    .then(() => objects);
}

function deleteFoto(name: string) {
  const dir = "uploads/" + name;
  return fs.unlink(dir);
}

function getImageNameByImageId(id: number): Promise<string> {
  let st: string;
  return getImages()
    .then((data) => {
      return data.find((imageInfo) => imageInfo.imageId === id);
    })
    .then((imageInfo) => {
      st = imageInfo.name;
      console.log(st);
      return st;
    });
}
// getImageNameById(1);

function getImagesByRealEstateId(id: number): Promise<ImageInfoConfig[]> {
  return getImages().then((data) =>
    data.filter((el) => el.idRealEstate === id)
  );
}

// getRealEstateObjects()
//   .then((data) => {
//     const newList = data.map((elem) => {
//       const newEl = { ...elem };
//       delete newEl["images"];
//       return newEl;
//     });
//     // const newList = data.map((elem) => ({ ...elem, images: [] }));
//     return newList;
//   })
//   .then((data) => {
//     setRealEstateObjects(data);
//   });

// setRealEstateObjects(REAL_ESTATE_OBJECTS);
