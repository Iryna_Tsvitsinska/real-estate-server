interface InitialStateConfig {
  currentList: RealEstateObjectTypeConfig[];
  totalLimits: LimitsConfig;
}

export enum UtilityConnectionType {
  NONE = -1,
  OWN = 0,
  CENTRALIZED = 1,
}
export enum OwnershipType {
  PRIVATE = 0,
  LEGAL = 1,
  RENT = 2,
  MUNICIPAL = 3,
}

export enum RealEstateType {
  GARAGE = 0,
  WAREHOUSE = 1,
  OFFICE = 2,
  FLAT = 3,
  HOUSE = 4,
}

export enum RealEstateStateType {
  RENT = 0,
  SALE = 1,
}

export type ActiveCategories = RealEstateType[];

export interface BasicRealEstateObjectConfig {
  fullSquare: number;
  activeSquare: number;
  gasType: UtilityConnectionType;
  waterType: UtilityConnectionType;
  sewerageType: UtilityConnectionType;
  electricityType: UtilityConnectionType;
  heatingType: UtilityConnectionType;
  floor: number;
  numberOfStoreys: number;
  totalBuildingStoreys: number;
  price: number;
  hasElevator: boolean;
  ownershipType: OwnershipType;
  propertyType: RealEstateType;
  propertyState: RealEstateStateType;
  id: number;
  description: string;
}

export interface GarageConfig extends BasicRealEstateObjectConfig {
  hasCellar: boolean;
  hasServicePit: boolean;
}

export interface WarehouseConfig extends BasicRealEstateObjectConfig {
  hasSecurity: boolean;
}

export interface OfficeConfig extends BasicRealEstateObjectConfig {
  hasParking: boolean;
}
export interface FlatConfige extends BasicRealEstateObjectConfig {
  hasBalcon: boolean;
}

export interface HouseConfig extends BasicRealEstateObjectConfig {
  gardenSquare: number;
}

export type RealEstateObjectTypeConfig =
  | GarageConfig
  | WarehouseConfig
  | OfficeConfig
  | FlatConfige
  | HouseConfig;

export interface LimitsConfig {
  minSquare: number;
  maxSquare: number;
  minPrice: number;
  maxPrice: number;
}

interface ResultSortConfig {
  sortList: RealEstateObjectTypeConfig[];
  searchLimits: LimitsConfig;
}

export interface ImageInfoConfig {
  imageId: number;
  name: string;
  idRealEstate: number;
}
